import { SequelizeOptions } from 'sequelize-typescript'

import { BlocksEntity } from '../models/blocks'
import { TxsEntity } from '../models/txs'

const ssl = process.env.PG_SSL === 'true'

export const databaseConfig: SequelizeOptions = {
  models: [
    BlocksEntity,
    TxsEntity,
  ],
  username: process.env.PG_USERNAME,
  password: process.env.PG_PASSWORD,
  database: process.env.PG_DB_NAME,
  host: process.env.PG_HOST,
  port: Number(process.env.PG_PORT),
  logging: false,
  dialect: 'postgres',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
  ssl,
  dialectOptions: {
    ssl,
  },
}
