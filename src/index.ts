import 'dotenv/config'

import { Sequelize } from 'sequelize-typescript'

import { databaseConfig } from './configs/database'

export const sequelize = new Sequelize(databaseConfig)

export * from './configs/database'
export * from './models/blocks'
export * from './models/txs'
