import { HasMany, AllowNull, Column, Model, CreatedAt, DataType, PrimaryKey, Table, UpdatedAt } from 'sequelize-typescript'

import { TxsEntity } from '../txs'

import { EBlockStatus } from './types'

@Table({
  tableName: 'blocks',
  timestamps: false,
})
export class BlocksEntity extends Model {
  @PrimaryKey
  @AllowNull(false)
  @Column(DataType.BIGINT)
  override id: string

  @Column
    hash: string

  @AllowNull(false)
  @Column(DataType.TEXT)
    status: EBlockStatus

  @CreatedAt
  @Column('timestamp with timestamp')
    created_at: Date

  @UpdatedAt
  @Column('timestamp with timestamp')
    updated_at: Date

  @HasMany(() => TxsEntity)
    _txs: TxsEntity[]
}
