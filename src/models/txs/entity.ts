import { ForeignKey, BelongsTo, AllowNull, AutoIncrement, Column, CreatedAt, DataType, Model, PrimaryKey, Table, UpdatedAt } from 'sequelize-typescript'

import { BlocksEntity } from '../blocks'

import { ETxStatuses } from './types'

@Table({
  tableName: 'transactions',
  timestamps: false,
})
export class TxsEntity extends Model {
  @PrimaryKey
  @AutoIncrement
  @AllowNull(false)
  @Column(DataType.BIGINT)
  override id: string

  @ForeignKey(() => BlocksEntity)
  @Column(DataType.BIGINT)
    block_id: string

  @AllowNull(false)
  @Column
    hash: string

  @AllowNull(false)
  @Column(DataType.TEXT)
    status: ETxStatuses

  @AllowNull(false)
  @Column
    from: string

  @Column
    to: string

  @AllowNull(false)
  @Column
    value: string

  @CreatedAt
  @Column('timestamp with timestamp')
    created_at: Date

  @UpdatedAt
  @Column('timestamp with timestamp')
    updated_at: Date

  @BelongsTo(() => BlocksEntity)
    _block: BlocksEntity
}
