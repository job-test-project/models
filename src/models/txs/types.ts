export enum ETxStatuses {
  FAILED = 'FAILED',
  SUCCESS = 'SUCCESS',
  UNKNOWN = 'UNKNOWN',
}
